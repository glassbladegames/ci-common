#!/bin/sh

set -eux

ls
zip -r archive.zip .
curl -f -H "JOB-TOKEN: $CI_JOB_TOKEN" -T archive.zip "${UPLOAD_URL}"
